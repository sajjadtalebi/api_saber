const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },

            colors: {
                pomegranate: {
                    50: "#FCF5F4",
                    100: "#F9EBEA",
                    200: "#EFCECA",
                    300: "#E6B0AA",
                    400: "#D3746B",
                    500: "#C0392B",
                    600: "#AD3327",
                    700: "#73221A",
                    800: "#561A13",
                    900: "#3A110D",
                },
                nephritis: {
                    50: "#F4FBF7",
                    100: "#E9F7EF",
                    200: "#C9EBD7",
                    300: "#A9DFBF",
                    400: "#68C690",
                    500: "#27AE60",
                    600: "#239D56",
                    700: "#17683A",
                    800: "#124E2B",
                    900: "#0C341D",
                },
            },
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
